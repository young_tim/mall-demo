export default {
  addCounter(state, payload) {
    // 购物车已有商品数量+1
    payload.count++
  },
  addToCart(state, payload) {
    payload.checked = true
    // 添加商品到购物车
    state.cartList.push(payload)
  }
}
