import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import FastClick from 'fastclick'
import VueLazyLoad from 'vue-lazyload'

import toast from 'components/common/toast'

Vue.config.productionTip = false

// 建立事件总线，管理事件。类似vuex之于状态。
Vue.prototype.$bus = new Vue()

/**
 * 安装插件
 */
// toast插件
Vue.use(toast)
// 图片懒加载插件：使用该插件，需要将 img:src 改为 img:v-lazy
Vue.use(VueLazyLoad, {
  loading: require('./assets/img/common/placeholder.png')
})

// 解决移动端300ms延迟
FastClick.attach(document.body)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
