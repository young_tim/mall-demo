# mall_demo

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### directory structure
- *node_modules*：node依赖包目录。`npm install` 后会自动下载依赖包，该目录一般仅在本地保存，不上传到服务器或git。
- *public*：公共资源文件目录。放在这里的资源文件，不会经过编译处理，会原样打包。
- *src*：开发核心目录。开发时，一般只会在这里处理代码。
  - *assets*：资源文件目录。小于设定阈值的图片，会编译为 `data64` 文件
  - *common*：公共js目录。
    - *const.js*：公共常量文件
    - *mixin.js*：混入内容管理文件
    - *utils.js*：公共工具函数文件
  - *components*：公共组件目录。
    - *common*：公共插件目录，可完全抽离并直接复用到其他项目。
    - *content*：与本项目有关的公共业务组件。
  - *network*：网络封装目录。
  - *router*：路由处理目录。
  - *store*：vuex状态管理目录。
  - *views*： 普通视图组件目录。
- *.browserslistrc*：浏览器兼容配置文件
- *.editorconfig*：代码编写规范指导文件
- *.babel.config.js*：ES6转ES5插件配置
- *.postcss.config.js：css编译配置文件，包括css单位转换插件
- *vue.config.js*：vue配置文件（vue cli 3），会与默认配置文件进行合并覆盖
