export default {
  addCart(content, payload) {
    return new Promise((resolve, repect) => {
      // 判断商品是否已存在
      let oldProduct = content.state.cartList.find(item => item.iid === payload.iid)
      if (oldProduct) {
        // 商品已存在，数量+1
        content.commit('addCounter', oldProduct)
        resolve('商品数量+1')
      } else {
        // 商品不存在，存入
        payload.count = 1
        content.commit('addToCart', payload)
        resolve('添加商品成功')
      }
    })
  }
}
