// vue配置文件（重载）
module.exports = {
  configureWebpack: {
    resolve: {
      // 配置目录别名
      alias: {
        // '@': 'src',  // 已默认配置
        'assets': '@/assets', // 相当于 src/assets
        'common': '@/common',
        'components': '@/components',
        'network': '@/network',
        'views': '@/views',
      }
    }
  }
}