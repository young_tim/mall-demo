import axios from 'axios'

export function request(config) {
  // 1.创建axios实例
  const instance = axios.create({
    baseURL: "http://127.0.0.1:8000/api", 
    timeout: 5000
  })

  // 2.启动axios的拦截器
  instance.interceptors.request.use(config => {
    // requestq请求拦截
    // 1.当发送网络请求时, 在页面中添加一个loading组件, 作为动画

    // 2.某些请求要求用户必须登录, 判断用户是否有token, 如果没有token跳转到login页面

    // 3.对请求的参数进行序列化(看服务器是否需要序列化)
    // config.data = qs.stringify(config.data)

    return config
  }, err => {
    if (err && err.response) {
      switch (err.response.status) {
        case 400:
          err.message = '请求错误'
          break
        case 401:
          err.message = '未授权的访问'
          break
      }
    }
    return err
  })

  instance.interceptors.response.use(res => {
    // response响应拦截
    return res.data
  }, err => {
    return err
  })

  // 3.发送真正的网络请求
  return instance(config)

}