import { debounce } from "./utils"

/**
 * 混入事件管理文件
 */

export const itemListenerMixin = {
  data () {
    return {
      itemImgListener: null
    }
  },
  mounted () {
    // 监听图片加载完成，并执行防抖refresh 
    let newRefresh = debounce(this.$refs.scroll.refresh, 100)
    this.itemImgListener = () => { newRefresh() }
    this.$bus.$on('itemImgLoad', this.itemImgListener)
  }
}

export const backTopMixin = {
  data () {
    return {
      isShowBackTop: false
    }
  },
  methods: {
    backTopClick() {
      this.$refs.scroll.scrollTo(0, 0, 500)
    },
  }
}
